#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <stdlib.h>

#define MAX_WORD_SIZE 15
#define MAX_WORDS 10
#define NBL 15
#define NBC 15

//3.Crossword generation



/* Loads a lexicon from a file.
 *    filename: the path to the file containing the lexicon
 *    lexicon: contains the lexicon when function terminates
 *    lexicon_size: contains the number of words of the lexicon when function terminates
 */


//fonction qui lit  un fichier et qui stocke les mots dans la matrice lexicon
void read_lexicon(const char* filename, char lexicon[MAX_WORDS][MAX_WORD_SIZE], int* lexicon_size) {
  FILE* lex_file;

  if((lex_file=fopen(filename, "r")) == NULL) {
    fprintf(stderr,"Cannot open file %s\n", filename);
    exit(1);
  }
  *lexicon_size=0;
  while(*lexicon_size<MAX_WORDS && fscanf(lex_file, "%s",lexicon[*lexicon_size])!=EOF)
    (*lexicon_size)++;
}


//stocke le 1er mot de lexicon dans longue
//puis compare avec tous les autres mots.
//Chaque fois qu'un mot plus long est trouvé, celui-ci remplace le mot présent dans longue
void longest(char lexicon[MAX_WORDS][MAX_WORD_SIZE],char longue[MAX_WORD_SIZE],int* indice_longest)
{
   
    for(int k=0;k<1;k++)
    {
        for(int l=0;lexicon[k][l] != '\0';l++)
        {
            longue[l]=lexicon[k][l];
        }
    }
    
    
    for(int i=1;i<MAX_WORDS;i++)
    {
        if(strlen(longue) < strlen(lexicon[i]))
        {
          *indice_longest = i;
            
            for(int j=0;j<MAX_WORD_SIZE;j++)
            {
                if(lexicon[i][j] != '\0')
                {
                    longue[j]=lexicon[i][j];
                }
                else
                {
                    break;
                }
            }
        }
    }
}


//remplit une matrice avec des caractères : ' ' 
void empty_mat(char grille[NBL][NBC])
{
    for(int i=0;i<NBL;i++)
    {
        for(int j=0;j<NBC;j++)
        {
            grille[i][j]=' ';
        }
    }
}


//remplit la ligne situé à la moitié de la grille par un mot
void place_longest_word(char grille[NBL][NBC],char word[MAX_WORD_SIZE])
{
    int size_word=strlen(word);
    int ligne_milieu=NBL/2;
    for(int i=0;i<size_word;i++)
    {
        grille[ligne_milieu][i]=word[i];
    }
}



//stocke le plus long mot dans longue et note sa position
//pour chaque mot du lexique, vérifie qu'il ne s'agit pas du mot le plus long (déjà placé dans la grille)
//puis stocke le mot dans le tabelau word
//bon_mot permet de savoir si le mot étudié est le mot le plus long ou non
void select_word(char lexicon[MAX_WORDS][MAX_WORD_SIZE],char word[MAX_WORD_SIZE],int num_word,int* indice_longest,int* bon_mot)
{
    char longue[MAX_WORD_SIZE]={0};
            
    longest(lexicon,longue,indice_longest);
    
    for(int i=num_word;i<=num_word;i++)
    {
        if (*indice_longest != i)
        {
            *bon_mot=1;
                           
            for(int j=0;j<MAX_WORD_SIZE;j++)
            {
                word[j]=lexicon[num_word][j];
            }
        }
        else
        {
            *bon_mot=0;
            break;
        }   
    }
}

//pour une lettre d'un mot, cherche parmi les lettres du mot déjà placé (le plus long)
//une lettre équivalente. Si c'est le cas on retient la position de la lettre dans le mot placé
//sinon on passe à la lettre suivante du mot que l'on cherche à placer
void research(char letter,char placed_word[MAX_WORD_SIZE],int* indice,int* result)
{
    for(int i=0;i<MAX_WORD_SIZE;i++)
    {
        if(letter == placed_word[i])
        {
            *indice=i;
            *result=1;
            break;
        }
    }
}

//conditions à remplir pour placer un mot verticalment
//on vérifie d'abord sur la colonne où on place s'il y a déjà un mot
//puis on regarde pour toutes les lignes si la colonne d'avant ou d'après contient un mot
int find_word_vertical(char grille[NBL][NBC],int indice,int début,int ligne_milieu)
{
    int cond1=1,cond2=1;
    int i=début;
    
    for(int i=début;i<NBL;i++)
    {
        if(grille[i][indice] != ' ')
        {
            if(i != ligne_milieu)
            {
                cond1=0;
                break;
            }
        }
    }
    
    if(cond1 == 0) return 0;
    
    for(int j=début;j<NBL;j++)
    {
        if(grille[j][indice+1] != ' ' || grille[j][indice-1] != ' ')
        {
            if(j != ligne_milieu)
            {
                cond2=0;
                break;
            }
        }
    }
    
    return cond2;
}

//place (ou non) un mot verticalment dans la grille
void fill_crossword_vertical(char grille[NBL][NBC],char word[MAX_WORD_SIZE],int indice,int* placed)
{
    int compt=0,espace=0,k=0,tmp=0;
    int ligne_milieu=NBL/2; //détermine la ligne au milieu de la grille
    int début=ligne_milieu; 
    espace = NBL - ligne_milieu; //on calcule l'espace disponible en soustrayant la ligne du milieu au nombre total de lignes de la grille
    
    *placed=0;
    
    if(word[0] != grille[début][indice])        //si la lettre à partir de laquelle on place le mot n'est pas la première du mot que l'on place
    {                                           //on décale la ligne à partir de laquelle on place le mot 
        while(word[k] != grille[début][indice]) //k est alors le nombre de lettres placées au dessus de la ligne du début (soit la ligne du milieu de la grille)
        {
            k++;
        }
        
        début=début-k;
    }        
    
    int resultat = find_word_vertical(grille,indice,début,ligne_milieu); //on vérifie les conditions pour savoir si le mot peut être placé
    
    if(resultat==1)
    {
        if(strlen(word)-k < espace)   //on vérifie que le mot n'est pas trop long et qu'il va rentrer dans la grille
        {
            *placed=1;
            for(int i=début;i<NBL;i++)
            {
                for(int j=compt;j<MAX_WORD_SIZE;j++)
                {
                    if(word[j] != '\0')
                    {   
                        grille[i][indice] = word[j];
                        compt++;
                        break;
                    }
                    else
                    {
                        break;
                    }
                }
            }
        }
    }
}



//conditions à remplir pour placer un mot horizontalement
int find_word2(char grille[NBL][NBC],int ligne_début,int colonne_début,int colonne_début_initial,char word[MAX_WORD_SIZE])
{
    int i=0;
    int cond1=1,cond2=1,cond3=1;
    
    while(word[i] != '\0')
    {
        i++;
    }
    
    if(colonne_début+i > NBC) return 0; //on vériffie que l'on ne sort pas de la grille
    
    for(int j=colonne_début;j<colonne_début+i;j++) //on vérifie qu'un mot ne soit pas déjà placé sur la ligne
    {       
        if(grille[ligne_début][j] != ' ')
        {
            if(j != colonne_début_initial)
            {
                cond1=0;
                break;
            }
        }
    }
    
    if(cond1 == 0) return 0;
    
    for(int k=colonne_début;k<colonne_début+i;k++) //on vérifie qu'il n'y ait pas d'autres lettres au-dessus et en-dessous du mot qu'on place
    {
        if(grille[ligne_début+1][k] != ' ' || grille[ligne_début-1][k] != ' ')
        {
            if(k != colonne_début_initial)
            {
                cond2=0;
                break;
            }
        }
    }
    
    if(cond2==0) return 0;
    
    if(grille[ligne_début][colonne_début-1] != ' ' || grille[ligne_début][colonne_début+i] != ' ')
    {
        cond3=0;
    }
    
    return cond3;
}


//place (ou non) un mot horizontalement dans la grille
void fill_crossword_horizontal(char grille[NBL][NBC],char word[MAX_WORD_SIZE],int indice_ligne,int indice_colonne,int* placed2)
{
    int colonne_début=0; 
    int colonne_début_initial=0;
    int ligne_début =0;
    int compt=0;
    int k=0;
    
    
    ligne_début=indice_ligne; //indice de la ligne sur laquelle on place le mot
    colonne_début=indice_colonne; //indice de la colonne à partir de laquelle le mot va débuté
    colonne_début_initial=colonne_début; //colonne initial à laquelle on a trouvé une lettre qui correspond
    
    *placed2=0;
    
    if(word[0] != grille[ligne_début][colonne_début])       //si la lettre à partir de laquelle on place le mot n'est pas la première du mot que l'on place
    {                                                       //on décale la colonne à partir de laquelle on place le mot
        while(word[k] != grille[ligne_début][colonne_début])
        {
            k++;
        }
        
        colonne_début=colonne_début-k;
    }
    
    if(colonne_début >= 0)      //on vérifie que la colonne à partir de laquelle on débute ne soit pas en ddehors de la grille
    {
        int resultat = find_word2(grille,ligne_début,colonne_début,colonne_début_initial,word); //on vérifie les conditions pour savoir si le mot peut être placé
            
        if(resultat == 1)
        {
            
            *placed2=1;
    
            for(int j=colonne_début;j<NBC;j++)
            {
                for(int i=compt;i<MAX_WORD_SIZE;j++)
                {   
                    if(word[i] != '\0')
                    {   
                        grille[ligne_début][j] = word[i];
                        compt++;
                        break;
                
                    }
                    else
                    {
                        break;
                    }
                }
            }
        }
    }
}


//pour placer un mot horizontalement, on regarde les lesttres correspondantes
//avec les mots placés verticalment
void compare_vertical(char grille[NBL][NBC],char word[MAX_WORD_SIZE])
{
    int placed2=0;
    int compt=0;
    
    for(int j=0;j<NBC;j++)
    {
        for(int i=0;i<NBL;i++)
        {
            if(grille[i][j] != ' ' && grille[i+1][j] !=' ')
            {
                for(int k=i;grille[k][j] != ' ';k++)
                {
                    for(int l=0;l<MAX_WORD_SIZE;l++)
                    {
                        if(word[l] == grille[k][j])
                        {
                            fill_crossword_horizontal(grille,word,k,j,&placed2);
                            break;
                        }
                    }
                    
                    if(placed2 == 1)
                    {
                        break;
                    }
                }
                
                break;
                
            }
        }
        
        if(placed2==1)
        {
            break;
        }
    }
}



//pour placer un mot verticalment, on regarde les lettres correspondantes
//avec le mot déjà placé horizontalement(le mot le plus long)
void compare_horizontal(char grille[NBL][NBC],char word[MAX_WORD_SIZE])
{
    int indice=-1;
    int result=0;
    int placed=0;
    int k=0;
    
    int ligne_milieu=NBL/2;
    
    
    for(int i=ligne_milieu;i<=ligne_milieu;i++)
    {
        for(int j=0;j<MAX_WORD_SIZE;j++)
        {
            result=0;
            
            while (result != 1)
            {
                research(word[k],grille[i],&indice,&result); //on regarde si la lettre est présente dans la grille
                k++;
            }
                        
            fill_crossword_vertical(grille,word,indice,&placed); //on place (ou non) le mot dans la grille
            
            if(placed == 1)
            {
                break;
            }
            
            
        }
        
        if(placed== 0)
        {
            compare_vertical(grille,word); // si le mot n'a pas été placé verticalment 
        }                                  //on regarde si on peut le placer horizontalement
    }        
}
                
//fonction principal qui prend les mots pour remplir la grille
void fill(char grille[NBL][NBC], char lexicon[MAX_WORDS][MAX_WORD_SIZE],char word[MAX_WORD_SIZE],int* indice_longest)
{
    int bon_mot; //permet de savoir si le mot sélectionné est le plus long
    
    for(int i=0;i<MAX_WORDS;i++)
    {
        bon_mot=0;
        select_word(lexicon,word,i,indice_longest,&bon_mot); //sélectionne le mot à la ligne i de la matrice lexicon
        
        if(bon_mot == 1)
        {      
           compare_horizontal(grille,word); //si le mot n'est le plus long (déjà placé) on enttre dans l'algorithme permettant de placer un mot
        }
    }
}


//affiche la grille                 
void print_empty_mat(char grille[NBL][NBC])
{
    for(int i=0;i<NBL;i++)
    {
        for(int j=0;j<NBC;j++)
        {
            printf("%c ",grille[i][j]);
        }
        printf("\n");
    }
}


//Programme principal : De la génération de la grille (vide) jusqu'à la fin du remplissage de celle_ci.

int main()
{
  int indice_longest = -1;  
  int lexicon_size;
  
  char crossword[NBL][NBC]; //matrice qui nous sert de grille  
  char lexicon[MAX_WORDS][MAX_WORD_SIZE]; //matrice qui permet de stocker les mots du fichier "lexiocn_used"
  char longue[MAX_WORD_SIZE]; //tableau stockant le plus long mot parmi ceux qui sont stockés dans lexicon
  char word[MAX_WORD_SIZE]; //tableau qui stocke 1 par 1 chaque mot de lexicon qui va être placé dans la grille
   
  
  read_lexicon("lexicon-american",lexicon,&lexicon_size); //stocke les mots du fichier "lexicon_used" dans la matrice lexicon
  
  longest(lexicon,longue,&indice_longest); //stocke le plus long mot de la liste dans le tableau longue
  
  empty_mat(crossword); //crée la grille à base de ' ' 
  
  place_longest_word(crossword,longue); //place le plus long mot (stocké dans longue) au milieu de la grille
  
  fill(crossword,lexicon,word,&indice_longest); //fonction principal de remplissage de la grille
  
  print_empty_mat(crossword); //affiche la grille
  
  return 0;
}
